commit ff110b2591f0bdeccd121c3726af19c62d6fb184
Author: Gabi Falk <gabifalk@gmx.com>
Date:   Tue Apr 30 20:05:04 2024 +0000

    Add a test to check for duplicate definitions in the static library
    
    This change follows two previous fixes addressing multiple definitions
    of __memcpy_chk and __mempcpy_chk functions on i586, and __memmove_chk
    and __memset_chk functions on i686.  The test is intended to prevent
    such issues from occurring in the future.
    
    Signed-off-by: Gabi Falk <gabifalk@gmx.com>
    Reviewed-by: H.J. Lu <hjl.tools@gmail.com>
    Reviewed-by: Dmitry V. Levin <ldv@altlinux.org>
    (cherry picked from commit ded2e0753e9c46debeb2e0d26c5e560d2581d314)

diff --git a/Makefile b/Makefile
index f7e4eb9ff2cc464c..37bf70aa4ad4403f 100644
--- a/Makefile
+++ b/Makefile
@@ -577,6 +577,13 @@ $(objpfx)lint-makefiles.out: scripts/lint-makefiles.sh
 	$(SHELL) $< "$(PYTHON)" `pwd` > $@ ; \
 	$(evaluate-test)
 
+# Link libc.a as a whole to verify that it does not contain multiple
+# definitions of any symbols.
+tests-special += $(objpfx)link-static-libc.out
+$(objpfx)link-static-libc.out:
+	$(LINK.o) $(whole-archive) -r $(objpfx)libc.a -o /dev/null > $@ 2>&1; \
+	$(evaluate-test)
+
 # Print test summary for tests in $1 .sum file;
 # $2 is optional test identifier.
 # Fail if there are unexpected failures in the test results.
