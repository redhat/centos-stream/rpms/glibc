summary: Test for bz580498 (pthread_rwlock_timedwrlock/rdlock() never return)
description: |
    Bug summary: pthread_rwlock_timedwrlock/rdlock() never return
    Bugzilla link: https://bugzilla.redhat.com/show_bug.cgi?id=580498

    Description:

    Different behavior between RHEL5 and RHEL6 in both pthread_rwlock_timedwrlock()
    and pthread_rwlock_timedrdlock() functions.

    If you call one of them with setting a negative number to abs_timeout->tv_sec,
    and a write lock to the specified rwlock has already been acquired by a different thread:

    RHEL 5) the function returns with ETIMEDOUT errno, but
    RHEL 6) the function never return (spinning).

    According to the man page and specs, EINVAL should be the return value on a negative timespec.


         EINVAL The  value  specified by rwlock does not refer to an initialized
                 read-write lock object, or the abs_timeout nanosecond  value  is
                 less than zero or greater than or equal to 1000 million.


    Apparently this change caused the issue:
     http://sourceware.org/ml/glibc-cvs/2009-q3/msg00036.html
contact: Miroslav Franc <mfranc@redhat.com>
component:
  - glibc
test: ./runtest.sh
framework: beakerlib
recommend:
  - glibc
  - gcc
tag:
  - RHEL61REVIEW
  - simple
  - noEWA
  - not-er15271
  - glibc-buildroot-ready
duration: 15m
link:
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=580498
extra-summary: /tools/glibc/Regression/bz580498-pthread-rwlock-timedwrlock-rdlock-never-return
extra-task: /tools/glibc/Regression/bz580498-pthread-rwlock-timedwrlock-rdlock-never-return
